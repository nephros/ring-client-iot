Name:       ring-client-iot

%define keepstatic 1

Summary:    CLI Client for Jami (GNU Ring)
Version:    1.0
Release:    0
License:    GPLv3+
URL:        https://github.com/SimpCosm/ring-client-iot
Source0:    %{name}-%{version}.tar.bz2
BuildRequires:  gcc-c++
BuildRequires:  jsoncpp
BuildRequires:  jsoncpp-devel
BuildRequires:  jami-daemon-devel

%description
%{summary}.

%prep
%autosetup -p1 -n %{name}-%{version}

%build
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%make_install

%files
%defattr(-,root,root,-)
%{_bindir}/*

